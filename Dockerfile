FROM python:3.8-alpine

# Create user with home directory and no password and change workdir
RUN adduser -Dh /webhook webhook
WORKDIR /webhook
# API will run on port 80
EXPOSE 8080

# Install bjoern and dependencies for install (we need to keep libev)
RUN apk add --no-cache --virtual .deps \
        musl-dev python3-dev gcc git && \
    apk add --no-cache libev-dev && \
    pip install bjoern


# Copy files to /webhook directory, install requirements
COPY ./ /webhook
RUN pip install -r /webhook/requirements.txt

# Cleanup dependencies
RUN apk del .deps

# Switch user
USER webhook

# Start bjoern
CMD ["python3", "-d", "server.py"]
