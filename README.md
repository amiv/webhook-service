
# AMIV Webhook service

## How to run this Flask app

### Development

To start the app locally for development, do the following:

1. Clone this repo
2. Create a python3 virtual environment: `virtualenv venv` (you might specify your python3 binary with `--pytho=/usr/bin/python3`)
3. Activate the virtual environment: `source venv/bin/activate`
4. Install the requirements inside the venv: `pip install -r requirements.txt`
5. Set the following environment variables: `export FLASK_APP="local.py"` and `export FLASK_DEBUG=1`
6. Create the configuration file with all the juicy secrets inside in `config.yaml`. You might copy the file `config.example.yaml`.
7. Run the flask app: `flask run`

### Production (w/o docker)

To start the app in a production environment, do the following:

1. clone this repo
2. Install the requirements: `pip install -r requirements.txt`
3. Set the following environment variable: `export FLASK_APP="run.py"`
4. Create the configuration file with all the juicy secrets inside in `config.yaml`. You might copy the file `config.example.yaml`.
5. Run the flask app: `python3 server.py`

### Production (w/ docker)

Create an Announce-Tool Backend service and give it access to the config using a docker secret:

```bash
# Create new Announce-Tool Backend service with secret
# Map port 80 (host) to 8080 (container)
docker service create \
    --name webhook-service  -p 80:8080 --network backend \
    -v data:\data -v -v config.yaml:config.yaml\
    amiveth/webhook-service
```

## Which endpoints are available

### /"hook_name"
Triggers the hook associated with "hook_name" given the token belongs to an authenticated user.

It expects three variables via POST:

1. "token" - The AMIV API token to be used for authentication



## Environment variables 
```
CONFIG -> name of config file (default: config.yaml)
```