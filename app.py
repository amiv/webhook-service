from flask import Flask, request, jsonify, send_file, abort
from flask_cors import CORS
from api import api
from yaml import safe_load
from os import getenv, makedirs
import os.path

with open(getenv('CONFIG', 'config.yaml')) as cfg:
  config = safe_load(cfg)

app = Flask(__name__)

app.config.update(
  API_URL=config.get('api_url', 'localhost'),
  HOOKS=config.get('hooks', [])
)

CORS(app)
api.init_app(app)
