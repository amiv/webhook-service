from flask import Flask, current_app
from flask_restplus import Namespace, Resource
from werkzeug.exceptions import BadRequest
import requests

from .auth.check_token import check_auth
import os.path

api = Namespace(
    'hook',
    description='Triggers hooks authenticated via AMIV groups')

upload_parser = api.parser()
upload_parser.add_argument('token', required=True)

@api.route('/<hook>')
class HookEndpoint(Resource):
    @api.expect(upload_parser)
    def post(self, hook):
        # Validate arguments
        args = upload_parser.parse_args()
        if not args.get('token'):
            BadRequest('No token submitted')
        token = args['token']

        # check requested hook
        c = current_app.config['HOOKS'].get(hook, False)
        if not c:
            raise BadRequest('Requested hook is not configured')

        groups = c['group'] if type(c['group']) == list else [c['group']]

        # check authorization for requested directory
        if not check_auth(token, groups):
            raise BadRequest('Not authorized to trigger this hook')
        
        # trigger hook
        res = requests.post(c['url'])

        return res.json(), res.status_code
