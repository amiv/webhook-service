from flask_restplus import Api, Resource
from flask import send_file, current_app
from .hook import api as hook
from werkzeug.exceptions import BadRequest

import os.path

api = Api(
    title='AMIV Webhook Service',
    description='Webhooks can be triggered via POST request, given the token belongs to a user of a group which is authorized',
)

api.add_namespace(hook)
